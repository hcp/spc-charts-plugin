package org.nrg.ccf.spc.conf;

import org.apache.log4j.Logger;
import org.nrg.framework.annotations.XnatPlugin;
import org.springframework.context.annotation.ComponentScan;

@XnatPlugin(
			value = "spcChartsPlugin",
			name = "SPC Charts Plugin"
		)
@ComponentScan({ 
	// None, for now
	})
public class SpcChartsPlugin {
	
	/** The logger. */
	public static Logger logger = Logger.getLogger(SpcChartsPlugin.class);

	/**
	 * Instantiates a new SPC charts plugin.
	 */
	public SpcChartsPlugin() {
		logger.info("Configuring SPC charts plugin");
	}
	
}
