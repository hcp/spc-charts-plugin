/*
 * Copyright (C) 2015 Washington University
 */
package org.nrg.xnat.restlet.presentation;
import java.util.*;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Predicate;
import org.apache.log4j.Logger;
import org.nrg.xdat.collections.DisplayFieldCollection.DisplayFieldNotFoundException;
import org.nrg.xdat.display.*;
import org.nrg.xdat.presentation.PresentationA;
import org.nrg.xdat.schema.SchemaElement;
import org.nrg.xdat.search.DisplayFieldWrapper;
import org.nrg.xdat.search.DisplaySearch;
import org.nrg.xdat.security.XDATUser;
import org.nrg.xft.XFTTable;
import org.nrg.xft.XFTTableI;
import org.nrg.xft.exception.ElementNotFoundException;
import org.nrg.xft.exception.XFTInitException;
import org.nrg.xft.schema.design.SchemaElementI;
import org.nrg.xft.utils.XftStringUtils;

/**
 * The Class RESTRawDataPresenter.
 */
public class RESTRawDataPresenter extends PresentationA {
    
    /** The logger. */
    static org.apache.log4j.Logger logger = Logger.getLogger(RESTRawDataPresenter.class);
    
    /* (non-Javadoc)
     * @see org.nrg.xdat.presentation.PresentationA#getVersionExtension()
     */
    public String getVersionExtension(){return "";}
    
    /** The server. */
    private String server = null;
    
    /** The search uri. */
    String searchURI=null;
    
    /** The user. */
    XDATUser user=null;
    
    /** The sort by. */
    public String sortBy=null;
    
    /** The tier. */
    public int tier = 0;
    
    /** The keep xsi names. */
    private boolean keepXsiNames = false;

    /**
     * Instantiates a new REST raw data presenter.
     *
     * @param serverLocal the server local
     * @param canClickHeaders the can click headers
     * @param searchURI the search uri
     * @param u the u
     * @param sortBy the sort by
     */
    public RESTRawDataPresenter(String serverLocal, boolean canClickHeaders,String searchURI,XDATUser u,String sortBy)
    {
        server = serverLocal;
        if (! server.endsWith("/"))
            server = server + "/";
        this.searchURI=searchURI;
        this.user=u;
        this.sortBy=sortBy;
    }

    /**
     * Instantiates a new REST raw data presenter.
     *
     * @param serverLocal the server local
     * @param searchURI the search uri
     * @param u the u
     * @param sortBy the sort by
     */
    public RESTRawDataPresenter(String serverLocal,String searchURI,XDATUser u,String sortBy)
    {
        server = serverLocal;
        if (! server.endsWith("/"))
            server = server + "/";
        this.searchURI=searchURI;
        this.user=u;
        this.sortBy=sortBy;
    }

    /**
     * Instantiates a new REST raw data presenter.
     *
     * @param serverLocal the server local
     * @param searchURI the search uri
     * @param u the u
     * @param sortBy the sort by
     * @param tier the tier
     * @param keepXsiNames the keep xsi names
     */
    public RESTRawDataPresenter(String serverLocal,String searchURI,XDATUser u,String sortBy, int tier, boolean keepXsiNames)
    {
    	this(serverLocal,searchURI,u,sortBy,tier);
    	this.keepXsiNames = keepXsiNames;
    }

    /**
     * Instantiates a new REST raw data presenter.
     *
     * @param serverLocal the server local
     * @param searchURI the search uri
     * @param u the u
     * @param sortBy the sort by
     * @param tier the tier
     */
    public RESTRawDataPresenter(String serverLocal,String searchURI,XDATUser u,String sortBy, int tier)
    {
        server = serverLocal;
        if (! server.endsWith("/"))
            server = server + "/";
        this.searchURI=searchURI;
        this.user=u;
        this.sortBy=sortBy;
        this.tier = tier;
    }


    /* (non-Javadoc)
     * @see org.nrg.xdat.presentation.PresentationA#formatTable(org.nrg.xft.XFTTableI, org.nrg.xdat.search.DisplaySearch)
     */
    public XFTTableI formatTable(XFTTableI table, DisplaySearch search) throws Exception
    {
        return formatTable(table,search,true);
    }
    
    /* (non-Javadoc)
     * @see org.nrg.xdat.presentation.PresentationA#formatTable(org.nrg.xft.XFTTableI, org.nrg.xdat.search.DisplaySearch, boolean)
     */
    public XFTTableI formatTable(final XFTTableI table,final DisplaySearch search,final boolean allowDiffs) throws Exception {
    	return formatTable(table,search,allowDiffs,true);
    }

    /**
     * Format table.
     *
     * @param table the table
     * @param search the search
     * @param allowDiffs the allow diffs
     * @param returnVisibleOnly the return visible only
     * @return the XFT table i
     * @throws Exception the exception
     */
    @SuppressWarnings({ "rawtypes", "unchecked" })
	public XFTTableI formatTable(final XFTTableI table, final DisplaySearch search,final boolean allowDiffs,final boolean returnVisibleOnly) throws Exception
    {
        logger.debug("BEGIN HTML FORMAT");
        XFTTable csv = new XFTTable();
        final List<DisplayFieldReferenceI> visibleFields = search.getVisibleFields(this.getVersionExtension());

        //need to remove fields that aren't visible (XDAT code needs to be fixed)
        CollectionUtils.filter(visibleFields, new Predicate() {
            @Override
            public boolean evaluate(Object arg0) {
                if(arg0 instanceof DisplayFieldWrapper){
                    try {
                        return ( !returnVisibleOnly || ((DisplayFieldWrapper)arg0).isVisible());
                    } catch (DisplayFieldNotFoundException e) {
                    }
                }
                return true;
            }
        });


		final ArrayList columnHeaders = new ArrayList();
		ArrayList diffs = new ArrayList();
		
		if (search.getInClauses().size()>0)
		{
		    for(int i=0;i<search.getInClauses().size();i++)
		    {
		        columnHeaders.add("");
		    }
		}
		
		//POPULATE HEADERS
		
		Iterator fields = visibleFields.iterator();
		int counter = search.getInClauses().size();
		while (fields.hasNext())
		{
			DisplayFieldReferenceI df = (DisplayFieldReferenceI)fields.next();
			if (allowDiffs)
			{
				if (!diffs.contains(df.getElementName()))
				{
				    diffs.add(df.getElementName());
				    SchemaElementI foreign = SchemaElement.GetElement(df.getElementName());
				    if (search.isMultipleRelationship(foreign))
				    {
					    String temp = XftStringUtils.SQLMaxCharsAbbr(search.getRootElement().getSQLName() + "_" + foreign.getSQLName() + "_DIFF");
					    Integer index = ((XFTTable)table).getColumnIndex(temp);
					    if (index!=null)
					    {
						    columnHeaders.add((keepXsiNames) ? df.getId() : "Diff");
					    }
				    }
				}
			}
			
			if (!df.isHtmlContent())
			{
				String value = df.getHeader();
				if ("ID".equals(value)){
					// Workaround for MS SYLK issue:
					// http://support.microsoft.com/kb/215591
					// http://nrg.wustl.edu/fogbugz/default.php?418
					value = value.toLowerCase();
				}
				columnHeaders.add((keepXsiNames) ? df.getId() : value);
			}
		}
		csv.initTable(columnHeaders);
		
		//POPULATE DATA
		table.resetRowCursor();
		
		while (table.hasMoreRows())
		{
			Hashtable row = table.nextRowHash();
			Object[] newRow = new Object[columnHeaders.size()];
			fields = visibleFields.iterator();

			diffs = new ArrayList();
			if (search.getInClauses().size()>0)
			{
			    for(int i=0;i<search.getInClauses().size();i++)
			    {
			        Object v = row.get("search_field"+i);
			        if (v!=null)
			        {
				        newRow[i] = v;
			        }else{
				        newRow[i] = "";
			        }
			    }
			}
			
			counter = search.getInClauses().size();
			while (fields.hasNext())
			{
			    DisplayFieldReferenceI dfr = (DisplayFieldReferenceI)fields.next();
				if(!dfr.isHtmlContent()) {

					try {
					    if (allowDiffs)
					    {
		                    if (!diffs.contains(dfr.getElementName()))
		                    {
		                        diffs.add(dfr.getElementName());
		                        SchemaElementI foreign = SchemaElement.GetElement(dfr.getElementName());
		                        if (search.isMultipleRelationship(foreign))
		                        {
		                    	    String temp = XftStringUtils.SQLMaxCharsAbbr(search.getRootElement().getSQLName() + "_" + foreign.getSQLName() + "_DIFF");
		                    	    Integer index = ((XFTTable)table).getColumnIndex(temp);
		                    	    if (index!=null)
		                    	    {
		                    		    String diff = "";
		                    		    Object d = row.get(temp.toLowerCase());
		                    	        if (d!=null)
		                    	        {
		                    		        diff=  (String)d.toString();
		                    	        }else{
		                    	            diff="";
		                    	        }
		                    		    newRow[counter++]=diff;
		                    	    }
		                        }
		                    }
					    }
	                    
	                    Object v = null;
	                    if (dfr.getElementName().equalsIgnoreCase(search.getRootElement().getFullXMLName()))
	                    {
	                    	v = row.get(dfr.getRowID().toLowerCase());
	                    }else{
	                    	v = row.get(dfr.getElementSQLName().toLowerCase() + "_" + dfr.getRowID().toLowerCase());
	                    }
	                    if (v != null)
	                    {
	                    	newRow[counter] = v;
	                    }
	                } catch (XFTInitException e) {
	                    logger.error("",e);
	                } catch (ElementNotFoundException e) {
	                    logger.error("",e);
	                }

					counter++;
				}
			}
			csv.insertRow(newRow);
		}
		
		logger.debug("END RAW DATA FORMAT");
        return csv;
    }
}
