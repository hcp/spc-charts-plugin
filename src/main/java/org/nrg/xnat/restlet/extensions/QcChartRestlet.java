/*
 * Copyright (C) 2015 Washington University
 */
package org.nrg.xnat.restlet.extensions;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.math.stat.StatUtils;
import org.apache.log4j.Logger;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.nrg.xdat.XDAT;
import org.nrg.xdat.om.XdatSearchFieldI;
import org.nrg.xdat.om.XnatProjectdata;
import org.nrg.xdat.search.DisplaySearch;
import org.nrg.xdat.security.helpers.Permissions;
import org.nrg.xdat.om.XdatStoredSearch;
import org.nrg.xft.XFTTableI;
import org.nrg.xnat.restlet.XnatRestlet;
import org.nrg.xnat.restlet.resources.SecureResource;
import org.restlet.Context;
import org.restlet.data.MediaType;
import org.restlet.data.Request;
import org.restlet.data.Response;
import org.restlet.data.Status;
import org.restlet.resource.StringRepresentation;
import org.restlet.resource.Variant;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

/**
 * Restlet used to filter and group cached search results.
 */
@XnatRestlet({"/projects/{PROJECT_ID}/qcchartdata","/projects/{PROJECT_ID}/qcchartconfig"})
public class QcChartRestlet  extends SecureResource { 

	/** The logger. */
	static org.apache.log4j.Logger logger = Logger.getLogger(QcChartRestlet.class);
	
	/** The proj. */
	private final XnatProjectdata proj;
	
	/** The Constant configTool. */
	private static final String configTool = "qc-chart";
	
	/** The Constant configSearchPath. */
	private static final String configSearchPath = "searches";
	
	/** The Constant configInfoFieldPath. */
	private static final String configInfoFieldPath = "info-fields";
	
	/** The Constant configInfoByField. */
	private static final String configInfoByField = "by-field";
	
	/** The Constant configInitialDisplayFieldPath. */
	private static final String configInitialDisplayFieldPath = "initial-display-fields";
	
	/** The keep numeric data only. */
	private final boolean keepNumericDataOnly;
	
	/**
	 * The Enum ChartTypes.
	 */
	// Currently only IR Charts supported, but likely to add more
	public enum ChartTypes {  
			
			/** The irchart. */
			IRCHART
	}
	
	/** The Constant DEFAULT_CHART. */
	private static final ChartTypes DEFAULT_CHART = ChartTypes.IRCHART;
	
	/** The chart type. */
	private final ChartTypes chartType;
	
	/** The Constant DEFAULT_INITIAL_BASELINE_NOBS. */
	private static final int DEFAULT_INITIAL_BASELINE_NOBS = 20;
	
	/** The Constant DEFAULT_BASELINE_NOBS. */
	private static final int DEFAULT_BASELINE_NOBS = 100;
	
	/** The baseline nobs. */
	private int baselineNobs;
	
	/** The baseline initial nobs. */
	private int baselineInitialNobs;
	
	/** The display nobs. */
	private int displayNobs;
	
	/**
	 * The Enum BaselinePeriods.
	 */
	public enum BaselinePeriods {  
		
		/** The Series beginning. */
		SeriesBeginning,
/** The Series end. */
SeriesEnd,
/** The Variable. */
Variable,
/** The Variable all. */
VariableAll
	}
	
	/** The Constant DEFAULT_BASELINE_PERIOD. */
	private static final BaselinePeriods DEFAULT_BASELINE_PERIOD = BaselinePeriods.SeriesEnd;
	
	/** The baseline period. */
	private final BaselinePeriods baselinePeriod;
    
    /** The proj db id. */
    private final Long projDbID;
    
    /** The info fields. */
    private final List<String> infoFields;
    
    /** The initial display fields. */
    private final List<String> initialDisplayFields;
    
    /** The by field. */
    private final String byField;
	
	/** The no by field string. */
	private final String noByFieldString = "NOBYVAR";
    
    /** The search by values. */
    private final Map<XdatStoredSearch,List<Object>> searchByValues = Maps.newLinkedHashMap();
	
	/** The cache. */
	private final static Map<String,ProjectCache> CACHE = new HashMap<>();
	private static ProjectCache _projectCache;
	private static Map<String,CachedSearchResults> _searchCache;
	private static Map<String,CachedChartJSON> _jsonCache;
	
	/** The Constant cacheName. */

	/**
	 * The Class OrganizedSearchResults.
	 */
	@SuppressWarnings("unused")
	private class OrganizedSearchResults {
		
		/** The charts. */
		public Map<String,Map<Object,ChartElements>> charts;
		
		/** The Session info fields. */
		public Map<String,Map<Object, List<String>>> SessionInfoFields;
		
		/** The initial display fields. */
		public List<String> initialDisplayFields;
	}
	
	
	private class ProjectCache implements Serializable {
		
		/**
		 * 
		 */
		private static final long serialVersionUID = 5245996606052176318L;
		final private Map<String,CachedSearchResults> _searchMap = new HashMap<>();
		final private Map<String,CachedChartJSON> _jsonMap = new HashMap<>();
		
		public Map<String, CachedSearchResults> getSearchMap() {
			return _searchMap;
		}
		
		public Map<String, CachedChartJSON> getJsonMap() {
			return _jsonMap;
		}
		
	}
	
	/**
	 * The Class CachedSearchResults.
	 */
	private class CachedSearchResults implements Serializable {
		
		private static final long serialVersionUID = 3852815812565429897L;

		/** The date. */
		public Date date;
		
		/** The ssx. */
		public XdatStoredSearch ssx;
		
		/** The t. */
		public XFTTableI t;
		
		/**
		 * Instantiates a new cached search results.
		 *
		 * @param date the date
		 * @param ssx the ssx
		 * @param t the t
		 */
		public CachedSearchResults(Date date,XdatStoredSearch ssx,XFTTableI t) {
			this.date = date;
			this.ssx = ssx;
			this.t = t;
		}
	}
	
	/**
	 * The Class CachedChartJSON.
	 */
	private class CachedChartJSON implements Serializable {
		
		/** The Constant serialVersionUID. */
		private static final long serialVersionUID = -5466025432214157699L;
		
		/** The date. */
		public Date date;
		
		/** The chart json. */
		public String chartJSON;
		
		/**
		 * Instantiates a new cached chart json.
		 *
		 * @param date the date
		 * @param chartJSON the chart json
		 */
		public CachedChartJSON(Date date,String chartJSON) {
			this.date = date;
			this.chartJSON = chartJSON;
		}
	}
	
	/**
	 * The Class ChartElements.
	 */
	@SuppressWarnings("unused")
	private class ChartElements {
		
		/** The Raw data. */
		public List<Double> RawData;
		
		/** The BaselineObs */
		public List<String> BaselineObs;
		
		/** The cl. */
		public List<Double> CL;
		
		/** The US d1. */
		public List<Double> USD1;
		
		/** The US d2. */
		public List<Double> USD2;
		
		/** The ucl. */
		public List<Double> UCL;
		
		/** The LS d1. */
		public List<Double> LSD1;
		
		/** The LS d2. */
		public List<Double> LSD2;
		
		/** The lcl. */
		public List<Double> LCL;
		
		/** The mr. */
		public List<Double> MR;
		
		/** The mr cl. */
		public List<Double> MR_CL;
		
		/** The M r_ us d1. */
		public List<Double> MR_USD1;
		
		/** The M r_ us d2. */
		public List<Double> MR_USD2;
		
		/** The mr ucl. */
		public List<Double> MR_UCL;
		
		/** The mr lcl. */
		public List<Double> MR_LCL;
		
		/** The Runs rules. */
		public List<String> RunsRules;
		
		/** The By value. */
		public String ByValue;
		
		/** The Chart title. */
		public String ChartTitle;
	}

	/**
	 * Instantiates a new qc chart restlet.
	 *
	 * @param context the context
	 * @param request the request
	 * @param response the response
	 */
	@SuppressWarnings("deprecation")
	public QcChartRestlet(Context context, Request request, Response response) {
		super(context, request, response);
		
		//cache.setMemoryStoreEvictionPolicy(new QcEvictionPolicy());
		
		final String projID = (String)getParameter(request,"PROJECT_ID");
		proj = (projID!=null) ? XnatProjectdata.getProjectByIDorAlias(projID, getUser(), false) : null;
		if (proj == null) {
			response.setStatus(Status.CLIENT_ERROR_NOT_FOUND,"Specified project either does not exist or user is not permitted access to subject data");
		}
		initializeCache(projID);
		this.getVariants().add(new Variant(MediaType.APPLICATION_JSON));
		this.getVariants().add(new Variant(MediaType.TEXT_PLAIN));
		
		// CHART PARAMETERS
		final String keepNumericStr = getQueryVariable("keepNumericDataOnly");
		keepNumericDataOnly = (keepNumericStr != null) ? !keepNumericStr.equalsIgnoreCase("false") : true; 
		
		final String clearCacheStr = getQueryVariable("clearCache");
		final boolean clearCache = (clearCacheStr != null) ? clearCacheStr.equalsIgnoreCase("true") : false; 
		if (clearCache) {
			_projectCache.getJsonMap().clear();
			_projectCache.getSearchMap().clear();
		} else {
			expireCacheIfNecessary();
		}
		
		final String chartTypeStr = getQueryVariable("chartType");
		// Currently only supporting IR Charts, but will likely add more
		if (chartTypeStr == null) {
			chartType=DEFAULT_CHART;
		} else if (chartTypeStr.equalsIgnoreCase("IRCHART")) {
			chartType=ChartTypes.IRCHART;
		} else {
			chartType=DEFAULT_CHART;
		}
		
		final String baselineNobsStr = getQueryVariable("baselineNobs");
		try {
			baselineNobs=(baselineNobsStr!=null) ? Integer.valueOf(baselineNobsStr) : DEFAULT_BASELINE_NOBS;
		} catch (NumberFormatException e) {
			baselineNobs=DEFAULT_BASELINE_NOBS;
		}
		
		final String baselineInitialNobsStr = getQueryVariable("baselineInitialNobs");
		try {
			baselineInitialNobs=(baselineInitialNobsStr!=null) ? Integer.valueOf(baselineInitialNobsStr) : DEFAULT_INITIAL_BASELINE_NOBS;
		} catch (NumberFormatException e) {
			baselineInitialNobs=DEFAULT_INITIAL_BASELINE_NOBS;
		}
		
		final String baselinePeriodStr = getQueryVariable("baselinePeriod");
		if (baselinePeriodStr == null) {
			baselinePeriod = DEFAULT_BASELINE_PERIOD;
		} else if (baselinePeriodStr.toLowerCase().contains("begin")) {
			baselinePeriod = BaselinePeriods.SeriesBeginning;
		} else if (baselinePeriodStr.toLowerCase().contains("variableall")) {
			baselinePeriod = BaselinePeriods.VariableAll;
		} else if (baselinePeriodStr.toLowerCase().contains("variable")) {
			baselinePeriod = BaselinePeriods.Variable;
		} else {
			baselinePeriod = BaselinePeriods.SeriesEnd;
		}
		
		final String displayNobsStr = getQueryVariable("displayNobs");
		displayNobs = (displayNobsStr == null || !isNumeric(displayNobsStr)) ? Integer.MAX_VALUE : Integer.parseInt(displayNobsStr);
		
        projDbID = (long)(Integer)proj.getItem().getProps().get("projectdata_info");
        
		final String chartInfoFieldsStr = XDAT.getConfigService().getConfigContents(configTool,configInfoFieldPath,projDbID);
		final String[] chartInfoFieldsA = (chartInfoFieldsStr != null && chartInfoFieldsStr.length()>0) ?
				chartInfoFieldsStr.replaceAll("[\n\\\\].*$","").split(",") : null;
		infoFields = (chartInfoFieldsA != null && chartInfoFieldsA.length>0) ? Arrays.asList(chartInfoFieldsA) : new ArrayList<String>();
        
		final String byFieldStr = XDAT.getConfigService().getConfigContents(configTool,configInfoByField,projDbID);
		byField = (byFieldStr!=null) ? byFieldStr.replaceAll("[\n\\\\].*$","") : null;
		
		final String initialDisplayFieldsStr = XDAT.getConfigService().getConfigContents(configTool,configInitialDisplayFieldPath,projDbID);
		final String[] initialDisplayFieldsA = (initialDisplayFieldsStr != null && initialDisplayFieldsStr.length()>0) ?
				initialDisplayFieldsStr.replaceAll("[\n\\\\].*$","").split(",") : null;
		initialDisplayFields = (initialDisplayFieldsA != null && initialDisplayFieldsA.length>0) ?
				Arrays.asList(initialDisplayFieldsA) : new ArrayList<String>();
		
	}

	private void expireCacheIfNecessary() {
		if (_projectCache.getSearchMap().size()<1) {
			return;
		}
		Calendar c = Calendar.getInstance();
		c.setTime(new Date());
		c.add(Calendar.DATE, -1);
		for (final CachedSearchResults cachedSearch : _projectCache.getSearchMap().values()) {
			if ((cachedSearch.date).before(c.getTime())) {
				_projectCache.getJsonMap().clear();
				_projectCache.getSearchMap().clear();
				break;
			}
		}
	}

	private void initializeCache(String projID) {
		if (!CACHE.containsKey(projID)) {
			CACHE.put(projID, new ProjectCache());
		}
		_projectCache = CACHE.get(projID);
		_jsonCache = _projectCache.getJsonMap();
		_searchCache = _projectCache.getSearchMap();
	}

	/* (non-Javadoc)
	 * @see org.restlet.resource.Resource#handleGet()
	 */
	@Override
	public void handleGet() {
		try {
			if (!proj.canRead(getUser())) {
				getResponse().setStatus(Status.CLIENT_ERROR_FORBIDDEN);
				return;
			}
		} catch (Exception e) {
			logger.error("Exception thrown",e);
			getResponse().setStatus(Status.SERVER_ERROR_INTERNAL);
			return;
		}
		if (getRequest().getResourceRef().getSegments().contains("qcchartdata")) {
			returnChartData();
		} else if (getRequest().getResourceRef().getSegments().contains("qcchartconfig")) {
			returnChartConfig();
		}
	}

	/**
	 * Return chart data.
	 */
	@SuppressWarnings("deprecation")
	private void returnChartData() {
		final String resourceRef = getRequest().getResourceRef().getPath() + "?" + getRequest().getResourceRef().getQuery().replaceAll("&_=.*$","");
		try {
			if (_jsonCache.containsKey(resourceRef)) {
				final CachedChartJSON cachedChartJSON = _jsonCache.get(resourceRef);
				logger.info("Using cached chart JSON  - " + resourceRef);
				if (cachedChartJSON.date !=null && ((new Date().getTime()-cachedChartJSON.date.getTime())<(Long.valueOf(24*60*60*1000))) &&
						cachedChartJSON.chartJSON != null) {
					getResponse().setEntity(new StringRepresentation(cachedChartJSON.chartJSON));
					getResponse().setStatus(Status.SUCCESS_OK);
					logger.info("Returning cached chart JSON  - " + resourceRef);
					return;
				}
				logger.error("ERROR:  cachedChartJSON could not be returned - " + resourceRef);
			}
		} catch (Exception e) {
			logger.error("ERROR:  Exception thrown accessing cachedChartJSON - " + resourceRef,e);
		}
		logger.info("No cachedChartJSON - " + resourceRef + ", Calculating chart data");
		final String chartConfig = XDAT.getConfigService().getConfigContents(configTool,configSearchPath,projDbID);
		final String[] chartSearchA = (chartConfig != null && chartConfig.length()>0) ? chartConfig.split(",") : null;
		final Map<String,OrganizedSearchResults> outMap = Maps.newLinkedHashMap();
		if (chartSearchA != null) {
			for (String ss : Arrays.asList(chartSearchA)) {
				ss = ss.replaceAll("[\n\\\\].*$","");
				final OrganizedSearchResults osr = new OrganizedSearchResults();
				osr.initialDisplayFields = initialDisplayFields;
				if (_searchCache.containsKey(ss)) {
					try {
						final CachedSearchResults cachedSearchResults = _searchCache.get(ss);
						//final CachedSearchResults cachedSearchResults = (CachedSearchResults)_searchCache.get(ss).getObjectValue();
						if (cachedSearchResults.date !=null && ((new Date().getTime()-cachedSearchResults.date.getTime())<(Long.valueOf(24*60*60*1000))) &&
								cachedSearchResults.t != null) {
							logger.info("Using CachedSearchResults - " + ss);
							osr.charts = getColumnMapFromTable(cachedSearchResults.t,cachedSearchResults.ssx);
							populateChartInfo(osr.charts,cachedSearchResults.ssx);
							computeChartData(osr.charts);
							// Note:  This call should come after the computeChartData method call.  Needs NOBS information
							osr.SessionInfoFields = getSessionInfoFields(cachedSearchResults.t,cachedSearchResults.ssx,osr.charts);
							outMap.put(ss,osr);
							continue;
						}
					} catch (Exception e) {
						logger.error("Exception thrown accessing search cache - " + ss, e);
					}
				}
				final XdatStoredSearch ssx = (ss.startsWith("@")) ? proj.getDefaultSearch(ss.substring(1)) : XdatStoredSearch.getXdatStoredSearchsById(ss,  getUser(),  true); 
				logger.info("No cached search available, running stored search - " + ss);
				if (ssx != null) {
					try {
						if (!(Permissions.canRead(getUser(),ssx) && Permissions.canRead(getUser(), proj))) {
							getResponse().setStatus(Status.CLIENT_ERROR_FORBIDDEN);
							return;
						}
						final DisplaySearch ds = ssx.getDisplaySearch(getUser());
						final XFTTableI t = ds.execute(getUser().getLogin());
						if (t != null) {
							osr.charts = getColumnMapFromTable(t,ssx);
							populateChartInfo(osr.charts,ssx);
							// Only cache reasonably sized results.
							if (t.getNumRows()<1000) {
								cacheSearchResults(ss, ssx, t);
							} else {
								logger.info("Search results too large to cache - " + ss);
							}
							computeChartData(osr.charts);
							// Note:  This call should come after the computeChartData method call.  Needs NOBS information
							osr.SessionInfoFields = getSessionInfoFields(t,ssx,osr.charts);
							outMap.put(ss,osr);
						} else {
							logger.error("Unexpectedly returned a null table (CLASS=" + this.getClass().getName() + ")");
							getResponse().setStatus(Status.SERVER_ERROR_INTERNAL);
							return;
						}
					} catch (Exception e) {
						logger.error("Exception thrown",e);
						getResponse().setStatus(Status.SERVER_ERROR_INTERNAL);
						return;
					}
				}
			}
		}
		final Gson gson = new GsonBuilder().create();
		final String gsonResults = (outMap.keySet().size()>=1) ? gson.toJson(outMap) : "";
		getResponse().setEntity(new StringRepresentation(gsonResults));
		getResponse().setStatus(Status.SUCCESS_OK);
		if (!_jsonCache.containsKey(resourceRef)) {
			logger.info("Adding chart data to cache - " + resourceRef);
			_jsonCache.put(resourceRef,new CachedChartJSON(new Date(),gsonResults));
		}
	}
	
	/**
	 * Cache search results.
	 *
	 * @param ss the ss
	 * @param ssx the ssx
	 * @param t the t
	 */
	private synchronized void cacheSearchResults(String ss, XdatStoredSearch ssx, XFTTableI t) {
		if (_searchCache != null) {
			final CachedSearchResults newCache = new CachedSearchResults(new Date(), ssx, t);
			logger.info("Writing search to cache - " + ss);
			_searchCache.put(ss,newCache);
		} else {
			logger.error("ERROR:  No cache available to store search - " + ss);
		}
	}

	/**
	 * Gets the session info fields.
	 *
	 * @param t the t
	 * @param ssx the ssx
	 * @param charts the charts
	 * @return the session info fields
	 */
	private Map<String, Map<Object,List<String>>> getSessionInfoFields(XFTTableI t, XdatStoredSearch ssx, Map<String, Map<Object,ChartElements>> charts) {
		t.resetRowCursor();
		final List<String> columnList = Lists.newArrayList(Arrays.asList(t.getColumns()));
		final Map<String, Map<Object,List<String>>> columnMap = Maps.newLinkedHashMap();
		final Iterator<String> it = columnList.iterator();
		while (it.hasNext()) {
			if (!infoFields.contains(it.next())) {
				it.remove();
			}
		}
		final Map<String,String> headerMap = Maps.newLinkedHashMap();
		for (final XdatSearchFieldI sf : ssx.getSearchField()) {
			headerMap.put(getTableColumnFromSearchField(ssx,sf),sf.getHeader());
		}
		for (final String column : columnList) {
			final Map<Object,List<String>> newMap = Maps.newLinkedHashMap();
			if (searchByValues.containsKey(ssx)) {
				for (final Object byValue : searchByValues.get(ssx)) {
					final List<String> newList = Lists.newArrayList();
					newMap.put(byValue, newList);
				}
			}
			columnMap.put((headerMap.containsKey(column)) ? headerMap.get(column) : column, newMap);
		}
		while (t.hasMoreRows()) {
			t.nextRow();
			for (final String column : columnList) {
				if (!columnMap.containsKey((headerMap.containsKey(column) ? headerMap.get(column) : column))) {
					continue;
				}
				final Object cellValue = t.getCellValue(column);
				final Object byValue = (byField == null || byField.length()<1 || !Arrays.asList(t.getColumns()).contains(byField)) ?
						noByFieldString : t.getCellValue(byField);
				if ( byValue == null) {
					continue;
				}
				if (cellValue == null) {
					final Map<Object, List<String>> byMap = columnMap.get((headerMap.containsKey(column)) ? headerMap.get(column) : column);
					if (byMap.containsKey(byValue)) {
						byMap.get(byValue).add(null);
					}
				} else {
					final Map<Object, List<String>> byMap = columnMap.get((headerMap.containsKey(column)) ? headerMap.get(column) : column);
					if (byMap.containsKey(byValue)) {
						byMap.get(byValue).add((t.getCellValue(column) != null) ? t.getCellValue(column).toString() : "");
						
					}
				}
			}
		}
		// trim lists back to display size
		//int displayNobs = getDisplayNobsFromChartData(charts);
		//if (displayNobs>=0) {
			// NOTE:  For now, we're assuming that all fields should have the same number of records (that should hold)
			for (final String column : columnMap.keySet()) {
				if (searchByValues.containsKey(ssx)) {
					for (final Object byValue : searchByValues.get(ssx)) {
						final List<String> columnValues = columnMap.get(column).get(byValue);
						final int currentSize = columnValues.size();
						if (charts.keySet().size()<1) continue;
						final Map<Object, ChartElements> chartValues = charts.get((charts.keySet().toArray())[0]);
						if (chartValues == null)  continue;
						final ChartElements chartByValue = chartValues.get(byValue);
						if (chartByValue == null) continue;
						for (int i=0; i<(currentSize-chartByValue.RawData.size()); i++) {
							columnValues.remove(0);
						}
					}
				}
			}
			return columnMap;
		//}
		//return null;
	}

	/**
	 * Gets the table column from search field.
	 *
	 * @param ssx the ssx
	 * @param sf the sf
	 * @return the table column from search field
	 */
	private String getTableColumnFromSearchField(XdatStoredSearch ssx, XdatSearchFieldI sf) {
		return (sf.getElementName().equals(ssx.getRootElementName())) ? sf.getFieldId().replaceAll("[:=]","_").toLowerCase() :
			sf.getElementName().replaceAll("[:=]","_").toLowerCase() + "_" + sf.getFieldId().replaceAll("[:=]","_").toLowerCase();
	}

	/*
	private int getDisplayNobsFromChartData(Map<String, Map<Object,ChartElements>> charts) {
		// Just verify that all charts have the same number of observations.
		int returnv = -9;
		for (final String chart : charts.keySet()) {
			for (final Object chartby : charts.get(chart).keySet()) {
				int thisSize = charts.get(chart).get(chartby).RawData.size();
				if (returnv>=0 && thisSize!=returnv) {
					return -9;
				} else {
					returnv = thisSize;
				}
			}
		}
		return returnv;
	}
	*/

	/**
	 * Populate chart info.
	 *
	 * @param map the map
	 * @param ssx the ssx
	 */
	private void populateChartInfo(Map<String, Map<Object,ChartElements>> map, XdatStoredSearch ssx) {
		// Note:  need to do case conversion on the header map right now.  The ChartElements object currently seems to have 
		// all field id's converted  to lower case, but we'll convert there too, in case that changes.
		final Map<String,String> headerMap = Maps.newLinkedHashMap();
		for (final XdatSearchFieldI sf : ssx.getSearchField()) {
			headerMap.put(getTableColumnFromSearchField(ssx,sf),sf.getHeader());
		}
		for (final String chartIt : map.keySet()) {
			for (final Object chartBy : map.get(chartIt).keySet()) {
				final ChartElements cEles = map.get(chartIt).get(chartBy);
				if (cEles != null) {
					cEles.ChartTitle = ((headerMap.containsKey(chartIt.toLowerCase())) ? headerMap.get(chartIt.toLowerCase()) : chartIt) +
							((! chartBy.equals(noByFieldString)) ? " (" + chartBy + ")" : "");
				}
			}
		}
	}

	/**
	 * Compute chart data.
	 *
	 * @param searchMap the search map
	 */
	private void computeChartData(Map<String, Map<Object,ChartElements>> searchMap) {
		for (final String ssit : searchMap.keySet()) {
			computeChartDataForItem(searchMap.get(ssit));
		}
	}

	/**
	 * Compute chart data for item.
	 *
	 * @param cElesMap the c eles map
	 */
	private void computeChartDataForItem(Map<Object,ChartElements> cElesMap) {
		if (chartType == ChartTypes.IRCHART) {
			computeIrChartData(cElesMap);
		}
	}

	
	/**
	 * Compute ir chart data.
	 *
	 * @param cElesMap the c eles map
	 */
	private void computeIrChartData(Map<Object,ChartElements> cElesMap) {
		logger.info("Calculating IR chart data");
		for (final Object byValue : cElesMap.keySet()) {
			final ChartElements cEles = cElesMap.get(byValue);
			final int calcBaselineNobs = getBaselineNobs(cEles);
			final List<Double> dataList = Lists.newArrayList(cEles.RawData);
			final List<String> baselineList = cEles.BaselineObs;
			// Step 1:  Compute Moving Range
			cEles.MR =  Lists.newArrayListWithCapacity(dataList.size());
			if (dataList.size()<2) {
				if (dataList.size()==1) { 
					cEles.MR.add(0,null);
				} 
				return;
			}
			// Initial MR entry will be null
			cEles.MR.add(0,null);
			// Intentionally starting at i=1
			for (int i=1; i<dataList.size(); i++) {
				if (dataList.get(i)==null || dataList.get(i-1)==null) {
					cEles.MR.add(i,null);
				} else if (isNumeric(dataList.get(i)) && isNumeric(dataList.get(i-1))) {
					cEles.MR.add(i,getAbsoluteDifference(dataList.get(i),dataList.get(i-1)));
				} else {
					cEles.MR.add(i,null);
				}
			}
			// Step 2:  Compute Data Mean
			final List<Double> baselineDataList = Lists.newArrayListWithCapacity(calcBaselineNobs);
			int startpoint=Integer.MAX_VALUE;
			if (baselinePeriod == BaselinePeriods.SeriesBeginning || baselinePeriod == BaselinePeriods.Variable ||
					baselinePeriod == BaselinePeriods.VariableAll) {
				for (int i=0; i<dataList.size(); i++) {
					if (dataList.get(i) != null) {
						startpoint = i;
						break;
					}
				}
			} else if (baselinePeriod == BaselinePeriods.SeriesEnd) {
				for (int i=dataList.size()-1; i>0; i--) {
					if (dataList.get(i) != null) {
						startpoint = i+1-(calcBaselineNobs);
						break;
					}
				}
			}
			if (startpoint==Integer.MAX_VALUE) {
				return;
			}
			for (int i=startpoint; i<(startpoint+calcBaselineNobs); i++) {
				if (dataList.get(i)!=null) {
					try {
						baselineDataList.add(Double.valueOf(dataList.get(i).toString()));
						if (i==startpoint) {
							baselineList.set(i, "S");
						} else if (i==(startpoint+calcBaselineNobs-1)) {
							baselineList.set(i, "E");
						} else {
							baselineList.set(i, "Y");
						}
					} catch (NumberFormatException e) {
						// Do nothing
					}
				}
			}
			final double[] baselineDataArr = new double[baselineDataList.size()];
			for (int i=0; i<baselineDataList.size(); i++) {
				baselineDataArr[i] = baselineDataList.get(i);
			}
			final double dataMean = StatUtils.mean(baselineDataArr);
			// Step 3:  Compute MR Mean
			final List<Double> baselineMrList = Lists.newArrayListWithCapacity(calcBaselineNobs);
			for (int i=startpoint; i<(startpoint+calcBaselineNobs); i++) {
				if (cEles.MR.get(i)!=null) {
					try {
						baselineMrList.add(Double.valueOf(cEles.MR.get(i).toString()));
					} catch (NumberFormatException e) {
						// Do nothing
					}
				}
			}
			final double[] baselineMrArr = new double[baselineMrList.size()];
			for (int i=0; i<baselineMrArr.length; i++) {
				baselineMrArr[i] = baselineMrList.get(i);
			}
			final double mrMean = StatUtils.mean(baselineMrArr);
			
			cEles.CL = Lists.newArrayListWithCapacity(dataList.size());
			cEles.CL.addAll(Collections.nCopies(dataList.size(),(Double)null));
			cEles.USD1 = Lists.newArrayListWithCapacity(dataList.size());
			cEles.USD1.addAll(Collections.nCopies(dataList.size(),(Double)null));
			cEles.USD2 = Lists.newArrayListWithCapacity(dataList.size());
			cEles.USD2.addAll(Collections.nCopies(dataList.size(),(Double)null));
			cEles.UCL = Lists.newArrayListWithCapacity(dataList.size());
			cEles.UCL.addAll(Collections.nCopies(dataList.size(),(Double)null));
			cEles.LSD1 = Lists.newArrayListWithCapacity(dataList.size());
			cEles.LSD1.addAll(Collections.nCopies(dataList.size(),(Double)null));
			cEles.LSD2 = Lists.newArrayListWithCapacity(dataList.size());
			cEles.LSD2.addAll(Collections.nCopies(dataList.size(),(Double)null));
			cEles.LCL = Lists.newArrayListWithCapacity(dataList.size());
			cEles.LCL.addAll(Collections.nCopies(dataList.size(),(Double)null));
			cEles.MR_CL = Lists.newArrayListWithCapacity(dataList.size());
			cEles.MR_CL.addAll(Collections.nCopies(dataList.size(),(Double)null));
			cEles.MR_USD1 = Lists.newArrayListWithCapacity(dataList.size());
			cEles.MR_USD1.addAll(Collections.nCopies(dataList.size(),(Double)null));
			cEles.MR_USD2 = Lists.newArrayListWithCapacity(dataList.size());
			cEles.MR_USD2.addAll(Collections.nCopies(dataList.size(),(Double)null));
			cEles.MR_UCL = Lists.newArrayListWithCapacity(dataList.size());
			cEles.MR_UCL.addAll(Collections.nCopies(dataList.size(),(Double)null));
			cEles.MR_LCL = Lists.newArrayListWithCapacity(dataList.size());
			cEles.MR_LCL.addAll(Collections.nCopies(dataList.size(),(Double)null));
			cEles.RunsRules = Lists.newArrayListWithCapacity(dataList.size());
			cEles.RunsRules.addAll(Collections.nCopies(dataList.size(),""));
			
			for (int i=0; i<cEles.RawData.size(); i++) {
				cEles.CL.set(i,dataMean);
				cEles.MR_CL.set(i,mrMean);
			}
			
			calculateLimits(cEles,0);
			calculateRunsRules(cEles);
			
			// In case anything goes wrong and there's a violation at the end that can't be eliminated (shouldn't happen - would be a bug)
			int justInCase = 0;
			while ((baselinePeriod == BaselinePeriods.Variable || baselinePeriod == BaselinePeriods.VariableAll) &&
					justInCase < 9999 && shiftLimits(cEles)) {
				justInCase++;
				calculateRunsRules(cEles);
			}
			
			trimListsBackToDisplaySize(cEles);
		}
	}

	/**
	 * Shift limits.
	 *
	 * @param cEles the c eles
	 * @return true, if successful
	 */
	private boolean shiftLimits(ChartElements cEles) {
		Integer rangeStart = null;
		Integer rangeStop = null; 
		@SuppressWarnings("unused")
		char ruleMatch = ' ';
		Integer rangeStart_D = null;
		Integer rangeStop_D = null;
		Integer obsCount_D = null;
		Integer rangeStart_C = null;
		Integer rangeStop_C = null;
		Integer obsCount_C = null;
		Integer rangeStart_B = null;
		Integer rangeStop_B = null;
		Integer obsCount_B = null;
		for (int i=0;i<cEles.RunsRules.size(); i++) {
			if (rangeStart_D==null && cEles.RunsRules.get(i).contains("D") && cEles.BaselineObs.get(i).equals("N")) {
				rangeStart_D=i;
				obsCount_D=1;
			} else if (rangeStart_D!=null && cEles.RawData.get(i)!=null) {
				if (cEles.RunsRules.get(i).contains("D")) {
					obsCount_D++;
				} else {
					rangeStart_D = null;
					obsCount_D = null;
				}
			}
			if (obsCount_D != null && obsCount_D==9) {
				rangeStop_D=i;
				break;
			}
		}
		if (baselinePeriod == BaselinePeriods.VariableAll) {
			for (int i=0;i<cEles.RunsRules.size(); i++) {
				if (rangeStart_C==null && cEles.RunsRules.get(i).contains("C") && cEles.BaselineObs.get(i).equals("N")) {
					if (rangeStart_D!=null && i>=rangeStart_D) {
						break;
					}
					rangeStart_C=i;
					obsCount_C=1;
				} else if (rangeStart_C!=null && cEles.RawData.get(i)!=null) {
					if (cEles.RunsRules.get(i).contains("C")) {
						obsCount_C++;
					} else {
						rangeStart_C = null;
						obsCount_C = null;
					}
				}
				if (obsCount_C != null && obsCount_C==5) {
					rangeStop_C=i;
					break;
				}
			}
			for (int i=0;i<cEles.RunsRules.size(); i++) {
				if (rangeStart_B==null && cEles.RunsRules.get(i).contains("B") && cEles.BaselineObs.get(i).equals("N")) {
					if ((rangeStart_C!=null && i>=rangeStart_C) || (rangeStart_D!=null && i>=rangeStart_D)) {
						break;
					}
					rangeStart_B=i;
					obsCount_B=1;
				} else if (rangeStart_B!=null && cEles.RawData.get(i)!=null) {
					if (cEles.RunsRules.get(i).contains("B")) {
						obsCount_B++;
					} else {
						rangeStart_B = null;
						obsCount_B = null;
					}
				}
				if (obsCount_B != null && obsCount_B==3) {
					rangeStop_B=i;
					break;
				}
			}
		}
		if ((rangeStart_D!=null) && ((rangeStart_C==null || rangeStart_C>=rangeStart_D) &&
				(rangeStart_B==null || rangeStart_B>=rangeStart_D))) {
			rangeStart = rangeStart_D;
			rangeStop = rangeStop_D;
			ruleMatch = 'D';
		} else if ((rangeStart_C!=null) && (rangeStart_B==null || rangeStart_B>=rangeStart_C)) {
			rangeStart = rangeStart_C;
			rangeStop = rangeStop_C;
			ruleMatch = 'C';
		} else if (rangeStart_B!=null) {
			rangeStart = rangeStart_B;
			rangeStop = rangeStop_B;
			ruleMatch = 'B';
		}
		if (rangeStart==null || rangeStop==null) {
			return false;
		}
		final double[] dataArr = new double[rangeStop-rangeStart+1];
		final double[] mrArr = new double[rangeStop-rangeStart+1];
		for (int i=rangeStart;i<=rangeStop;i++) {
			if (i == rangeStart) {
				cEles.BaselineObs.set(i,"S");
			} else if (i == rangeStop) {
				cEles.BaselineObs.set(i,"E");
			} else {
				cEles.BaselineObs.set(i,"Y");
			}
			if (cEles.RawData != null && cEles.RawData.get(i)!=null) {
				dataArr[i-rangeStart] = cEles.RawData.get(i);
			}
			if (cEles.MR != null && cEles.MR.get(i)!=null) {
				mrArr[i-rangeStart] = cEles.MR.get(i);
			}
		}
		final double dataMean = StatUtils.mean(dataArr);
		final double mrMean = StatUtils.mean(mrArr);
		for (int i=rangeStart;i<cEles.RawData.size();i++) {
			cEles.CL.set(i,dataMean);
			cEles.MR_CL.set(i,mrMean);
		}
		calculateLimits(cEles,rangeStart);
		return true;
	}

	/*
	private boolean needsShift(ChartElements cEles) {
		if (baselinePeriod == BaselinePeriods.Variable) {
			for (final String viol : cEles.RunsRules) {
				if (viol.contains("D")) {
					return true;
				}
			}
		} else if (baselinePeriod == BaselinePeriods.VariableAll) {
			for (final String viol : cEles.RunsRules) {
				if (viol != null && viol.length()>0) {
					return true;
				}
			}
		}
		return false;
	}
	*/

	/**
	 * Calculate runs rules.
	 *
	 * @param cEles the c eles
	 */
	private void calculateRunsRules(ChartElements cEles) {
			// Clear out any current runs rules
			for (int i=(cEles.RunsRules.size()-1); i>0; i--) {
				if (cEles.RunsRules.get(i)==null || !cEles.RunsRules.get(i).equals("")) {
					cEles.RunsRules.set(i,"");
				}
			}
			// Step ##:  Flag rules violations (Currently Western Electric Rules)
			for (int i=1;i<cEles.RawData.size(); i++) {
				if (cEles.RawData.get(i)==null) {
					continue;
				}
				boolean returnVar = false;
				int returnCount = 0;
				if (returnCount>0) {
					returnCount++;
				}
				// RULE A - One point outside confidence intervals
				if (cEles.RawData.get(i) > cEles.UCL.get(i) || cEles.RawData.get(i) < cEles.LCL.get(i)) {
					runsRuleAppend(cEles.RunsRules,i,1,'A');
				}
				// RULE B - 2 out of 3 in zone A or beyond (beyond 2-sigma)
				int aboveZoneA = 0;
				int belowZoneA = 0;
				int obsBackA = 0;
				int endVar = 3;
				for (int j=i; j>(i-endVar); j--) {
					if (j<1) {
						continue;
					} else if (cEles.RawData.get(j) == null || cEles.USD2.get(j) == null) {
						endVar++;
						continue;
					} else if (j<i && !cEles.CL.get(j).equals(cEles.CL.get(j+1))) {
						// Quit processing if there's been a shift in control limits during this check;
						break;
					}
					if (cEles.RawData.get(j) > cEles.USD2.get(j)) {
						aboveZoneA++;
						obsBackA++;
					} else if (cEles.RawData.get(j) < cEles.LSD2.get(j)) {
						belowZoneA++;
						obsBackA++;
					} else if (j > (i-2)) {
						// Always count all observations but the last one
						obsBackA++;
					}
				}
				if (belowZoneA>=2 || aboveZoneA>=2) {
					runsRuleAppend(cEles.RunsRules,i,obsBackA,'B');
					if (baselinePeriod==BaselinePeriods.VariableAll) {
						// Basically done (this violation will cause shift), but we'll go out a few more points to make 
						// sure the longest rule violation gets used for limits shifts
						returnVar = true;
						returnCount = 1;
					}
				}
				// RULE C - 4 out of 5 in zone B or beyond (beyond 1-sigma)
				int aboveZoneB = 0;
				int belowZoneB = 0;
				int obsBackB =	 0;
				endVar = 5;
				for (int j=i; j>(i-endVar); j--) {
					if (j<1) {
						continue;
					} else if (cEles.RawData.get(j) == null || cEles.USD1.get(j) == null) {
						endVar++;
						continue;
					} else if (j<i && !cEles.CL.get(j).equals(cEles.CL.get(j+1))) {
						// Quit processing if there's been a shift in control limits during this check;
						break;
					}
					if (cEles.RawData.get(j) > cEles.USD1.get(j)) {
						aboveZoneB++;
						obsBackB++;
					} else if (cEles.RawData.get(j) < cEles.LSD1.get(j)) {
						belowZoneB++;
						obsBackB++;
					} else if (j > (i-4)) {
						// Always count all observations but the last one
						obsBackB++;
					}
				}
				if (belowZoneB >= 4 || aboveZoneB >= 4) {
					runsRuleAppend(cEles.RunsRules,i,obsBackB,'C');
					if (baselinePeriod==BaselinePeriods.VariableAll) {
						// Basically done (this violation will cause shift), but we'll go out a few more points to make 
						// sure the longest rule violation gets used for limits shifts
						returnVar = true;
						returnCount = 1;
					}
				}
				// RULE D - 9 in a row on one side of center line
				int aboveCL = 0;
				int belowCL = 0;
				endVar = 9;
				for (int j=i; j > (i-endVar); j--) {
					if (j<1) {
						continue;
					} else if  (cEles.RawData.get(j) == null || cEles.CL.get(j) == null) {
						endVar++;
						continue;
					} else if (j<i && !cEles.CL.get(j).equals(cEles.CL.get(j+1))) {
						// Quit processing if there's been a shift in control limits during this check;
						break;
					}
					if (cEles.RawData.get(j) > cEles.CL.get(j)) {
						aboveCL++;
					} else if (cEles.RawData.get(j) < cEles.CL.get(j)) {
						belowCL++;
					}
				}
				if (belowCL == 9 || aboveCL == 9) {
					runsRuleAppend(cEles.RunsRules,i,9,'D');
					if (baselinePeriod==BaselinePeriods.Variable || baselinePeriod==BaselinePeriods.VariableAll) {
						// Basically done (this violation will cause shift), but we'll go out a few more points to make 
						// sure the longest rule violation gets used for limits shifts
						returnVar = true;
						returnCount = 1;
					}
				}
				if (returnVar & returnCount>9) {
					return;
				}
			}
		
	}
	
	/**
	 * Trim lists back to display size.
	 *
	 * @param cEles the c eles
	 */
	private void trimListsBackToDisplaySize(ChartElements cEles) {
		
			if (cEles.RawData.size() > displayNobs) {
				final int currentSize = cEles.RawData.size();
				for (int i=0; i<(currentSize-displayNobs); i++) {
					cEles.RawData.remove(0);
					cEles.BaselineObs.remove(0);
					cEles.CL.remove(0);
					cEles.UCL.remove(0);
					cEles.USD1.remove(0);
					cEles.USD2.remove(0);
					cEles.LCL.remove(0);
					cEles.LSD1.remove(0);
					cEles.LSD2.remove(0);
					cEles.MR.remove(0);
					cEles.MR_CL.remove(0);
					cEles.MR_UCL.remove(0);
					cEles.MR_USD1.remove(0);
					cEles.MR_USD2.remove(0);
					cEles.MR_LCL.remove(0);
					cEles.RunsRules.remove(0);
				}
			}
		
	}

	/**
	 * Calculate limits.
	 *
	 * @param cEles the c eles
	 * @param startPoint the start point
	 */
	private void calculateLimits(ChartElements cEles,Integer startPoint) {
		for (int i=((startPoint==null) ? 0 : startPoint); i<cEles.RawData.size(); i++) {
			final double dataMean = cEles.CL.get(i);
			final double mrMean = cEles.MR_CL.get(i);
			final double symClValue = 2.66*mrMean;
			final double assymClValue = 3.267*mrMean;
			cEles.UCL.set(i,dataMean+symClValue);
			cEles.USD1.set(i,dataMean+(symClValue/3));
			cEles.USD2.set(i,dataMean+(symClValue/3*2));
			cEles.LCL.set(i,dataMean-symClValue);
			cEles.LSD1.set(i,dataMean-(symClValue/3));
			cEles.LSD2.set(i,dataMean-(symClValue/3*2));
			cEles.MR_UCL.set(i,mrMean+(assymClValue));
			cEles.MR_USD1.set(i,mrMean+(assymClValue/3));
			cEles.MR_USD2.set(i,mrMean+(assymClValue/3*2));
			cEles.MR_LCL.set(i,Double.valueOf(0));
		}
	}

	/**
	 * Runs rule append.
	 *
	 * @param rrList the rr list
	 * @param pointer the pointer
	 * @param nObs the n obs
	 * @param rule the rule
	 */
	private void runsRuleAppend(List<String> rrList, int pointer, int nObs, char rule) {
		for (int i=pointer; i>(pointer-nObs); i--) {
			if (i<1) {
				continue;
			}
			final String rrStr = (String)rrList.get(i);
			final StringBuilder sb = (rrStr==null) ? new StringBuilder() : new StringBuilder(rrStr);
			if (!(rrStr.indexOf(rule)>=0)) {
				if (sb.length()>0) {
					sb.append(',');
				} 
				sb.append(rule);
			}
			rrList.set(i,sb.toString());
		}
	}

	/**
	 * Gets the baseline nobs.
	 *
	 * @param cEles the c eles
	 * @return the baseline nobs
	 */
	private int getBaselineNobs(ChartElements cEles) {
		final List<Double> rawDataList = (List<Double>)cEles.RawData;
		final int calcBaselineNobs = (rawDataList.size()>=baselineNobs && !baselinePeriod.toString().contains("Variable")) ? baselineNobs :
								(rawDataList.size()>=baselineInitialNobs) ? baselineInitialNobs : rawDataList.size();  
		return calcBaselineNobs;
	}

	/**
	 * Gets the absolute difference.
	 *
	 * @param inobj1 the inobj1
	 * @param inobj2 the inobj2
	 * @return the absolute difference
	 */
	private Double getAbsoluteDifference(Double inobj1, Double inobj2) {
		if (inobj1==null || inobj2==null) {
			return null;
		} else {
			return  Math.abs(inobj1-inobj2);
		}
	}
	
	/*
	private Number getAbsoluteDifference(Object inobj1, Object inobj2) {
		try {
			final Object num1;
			final Object num2;
			// Don't expect this to happen in this data, but check for it just the same
			if (!inobj1.getClass().equals(inobj2.getClass())) {
				num1 = inobj1.toString();
				num2 = inobj2.toString();
			} else {
				num1 = inobj1;
				num2 = inobj2;
			}
			if (num1 instanceof Integer) {
				return Math.abs((Integer)num1-(Integer)num2);
			} else if (num1 instanceof Float) {
				return Math.abs((Float)num1-(Float)num2);
			} else if (num1 instanceof Double) {
				return Math.abs((Double)num1-(Double)num2);
			} else if (num1 instanceof Long) {
				return Math.abs((Long)num1-(Long)num2);
			} else if (num1 instanceof String) {
				if (((String)num1).contains(".") || ((String)num2).contains(".")) {
					return Math.abs(Double.parseDouble((String)num1)-Double.parseDouble((String)num2));
				} else {
					return Math.abs(Long.parseLong((String)num1)-Long.parseLong((String)num2));
				}
			} else {
				// Shouldn't ever hit this block, but if we do, try converting to string and calculating.
				if (num1.toString().contains(".") || num2.toString().contains(".")) {
					return Math.abs(Double.parseDouble(num1.toString())-Double.parseDouble(num2.toString()));
				} else {
					return Math.abs(Long.parseLong(num1.toString())-Long.parseLong(num2.toString()));
				}
			}
		} catch (NumberFormatException e) {
			// Do nothing
		}
		return null;
	}
	*/

	/**
	 * Return chart config.
	 */
	@SuppressWarnings("deprecation")
	private void returnChartConfig() {
        final Long projID = (long)(Integer)proj.getItem().getProps().get("projectdata_info");
		getResponse().setEntity(new StringRepresentation(XDAT.getConfigService().getConfigContents(configTool,configSearchPath,projID)));
		getResponse().setStatus(Status.SUCCESS_OK);
	}
	
	/**
	 * Gets the column map from table.
	 *
	 * @param t the t
	 * @param ssx the ssx
	 * @return the column map from table
	 */
	private Map<String, Map<Object,ChartElements>> getColumnMapFromTable(XFTTableI t, XdatStoredSearch ssx) {
		t.resetRowCursor();
		final Map<String, Map<Object,ChartElements>> columnMap = Maps.newLinkedHashMap();
		final List<String> columnList = Lists.newArrayList(Arrays.asList(t.getColumns()));
		final List<Object> byValues = Lists.newArrayList();
		searchByValues.put(ssx,byValues);
		final Iterator<String> it = columnList.iterator();
		while (it.hasNext()) {
			final String column = it.next();
			if (infoFields.contains(column.toLowerCase())) {
				it.remove();
			}
		}	
		if (byField == null || byField.length()<1 || !columnList.contains(byField)) {
			byValues.add(noByFieldString);
		} else {
			while (t.hasMoreRows()) {
				t.nextRow();
				final Object cellValue = t.getCellValue(byField);
				if (!byValues.contains(cellValue)) {
					byValues.add(cellValue);
				}
			}
			t.resetRowCursor();
		}
		Collections.sort(byValues,new Comparator<Object>(){
			@Override
			public int compare(Object arg0, Object arg1) {
				return arg0.toString().compareTo(arg1.toString());
			}
		});
		for (final String column : columnList) {
			final Map<Object,ChartElements> cElesMap = Maps.newLinkedHashMap();
			for (final Object byValue : byValues) {
				final ChartElements cEles = new ChartElements();
				final List<Double> newList = Lists.newArrayList(); 
				cEles.RawData = newList;
				final List<String> baselineList = Lists.newArrayList(); 
				cEles.BaselineObs = baselineList;
				cElesMap.put(byValue, cEles);
			}
			columnMap.put(column, cElesMap);
		}
		while (t.hasMoreRows()) {
			t.nextRow();
			for (final String column : columnList) {
				final Object cellValue = t.getCellValue(column);
				final Object byValue = (!byValues.get(0).equals(noByFieldString)) ? t.getCellValue(byField) : noByFieldString;
				if (byValue == null) {
					continue;
				}
				columnMap.get(column).get(byValue).BaselineObs.add("N");
				if (cellValue == null) {
					columnMap.get(column).get(byValue).RawData.add(null);
				} else if (cellValue instanceof Double) {
					columnMap.get(column).get(byValue).RawData.add((Double)t.getCellValue(column));
				} else if (isNumeric(cellValue)) {
					columnMap.get(column).get(byValue).RawData.add(Double.valueOf((t.getCellValue(column)).toString()));
				} else {
					columnMap.get(column).get(byValue).RawData.add(null);
				}
			}
		}
		// Remove columns from map with non-numeric values (reverse list, so we're looking at latest values)
		for (final String column : Lists.reverse(Lists.newArrayList(columnList))) {
			for (final Object byValue : byValues) {
				final List<Double> colValues = (List<Double>)columnMap.get(column).get(byValue).RawData;
				double nullCount=0;
				final double rowsToCount = Math.ceil(colValues.size()/10);
				for (int i=0;i<=rowsToCount;i++) {
					if (colValues.get(i) == null) {
						nullCount++;
					} else if (keepNumericDataOnly && !isNumeric(colValues.get(i))) {
						columnMap.get(column).remove(byValue);
						break;
					}
					// Remove columns that are effectively all null
					if (keepNumericDataOnly && (nullCount>=Math.floor(rowsToCount*.99))) {
						columnMap.get(column).remove(byValue);
						break;
					}
				}
			}
			if (columnMap.get(column).keySet().size()<1) {
				columnMap.remove(column);
			}
		}
		return columnMap;
	}
	
	/**
	 * Checks if is numeric.
	 *
	 * @param o the o
	 * @return true, if is numeric
	 */
	private boolean isNumeric(Object o) {
		// NOTE:  Returning true for null values because we don't want to drop based on null (Currently won't check nulls anyway, but just in case....)
		return (o==null) ? true : o.toString().matches("-?\\d+(\\.\\d+)?");
	}
		
}
