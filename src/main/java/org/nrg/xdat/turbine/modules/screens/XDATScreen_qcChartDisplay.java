/*
 * org.nrg.xnat.turbine.modules.screens.XDATScreen_scanTypeCleanup
 * XNAT http://www.xnat.org
 * Copyright (c) 2014, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 *
 * Last modified 7/10/13 9:04 PM
 */
package org.nrg.xdat.turbine.modules.screens;

import org.apache.log4j.Logger;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;
import org.nrg.xdat.turbine.modules.screens.SecureReport;

public class XDATScreen_qcChartDisplay extends SecureReport {
     static org.apache.log4j.Logger logger = Logger.getLogger(XDATScreen_qcChartDisplay.class);

	@Override
	public void finalProcessing(RunData data, Context context) {
	}
     
 }
